# chatbot 

# problem statement
version 1:
1. Build an App that can chat with a botserver.
2. Single screen similar to a whatsapp chat screen
3. with edit box and send button at the bottom of the screen.
4. Connect external API of Chat server to send & receive messages 

version 2: [attempt ver2.0 only after ver1.0 is completed in all respects]
1. Store and display past history messages - both sent and received
2. Add a hamburger menu equivalent and have multiple chat screens on click of list

version 3: [attempt ver3.0 only after ver2.0 is completed in all respects]
1. Get the app to work offline - where message is stored locally and sent to server once
internet is on

ChatScreen UI:

1. Navigation bar with title as ChatBot
2. UITextField with a SEND Button at the bottom of the screen. Which scrolls up when
keyboard appears.
3. In the middle chat messages to be displayed in chat bubbles - sent messages on the
right and received messages


### My Approach ###
* Skipping DI for now to hustle up
* Implementing MVVM 
* ViewModel to survive the state during orientation change
* LiveData for reactive bindings
* Coroutines for async tasks
* Moshi for kotlin friendly json parsing

### TO DO ###
* show time in chat message
* if message is small , received message alignment is not proper
* rather observe whole table message, need to listen only specific chatbot message changes from database
* activity state need to save in case of config changes


