package docsapp.chatbot.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import docsapp.chatbot.R
import kotlinx.android.synthetic.main.fragment_chatbot_list.view.*

// show list of chat bot(available)


class ChatBotListingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chatbot_list, container, false)

        // make dummy chat bot as in the problem only one is given
        val chatBotList = listOf("ChatBot 1","ChatBot 2","ChatBot 3")
        val arrayAdapter  = ArrayAdapter(activity,android.R.layout.simple_expandable_list_item_1,chatBotList)
        view.chat_bot_list.adapter = arrayAdapter

        view.chat_bot_list.setOnItemClickListener { parent, view, position, id ->
            activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container,
                    ChatBotFragment.newInstance((position+1).toLong())
                ).addToBackStack("Main").commit()

        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance( ) =
            ChatBotListingFragment()
    }
}
