package docsapp.chatbot.ui

/**
 * Created by Vikas Gupta on 7/7/19.
 */

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import docsapp.chatbot.R
import docsapp.chatbot.data.local.database.MessageEntity
import kotlinx.android.synthetic.main.fragment_chatbot_item.view.*


class MessageAdapter(
) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

     var messageList: List<MessageEntity>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_chatbot_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = messageList!![position]
        if(item.isReceived){
            holder.messageTv.gravity = Gravity.RIGHT
        }
        holder.messageTv.text = "${item.message}"
    }

    override fun getItemCount(): Int {
        if(messageList == null) {
            return 0
        } else {
           return messageList!!.size
        }
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val messageTv: TextView = mView.message_tv
        //  val mContentView: TextView = mView.content

        /*    override fun toString(): String {
                return super.toString() + " '" + mContentView.text + "'"
            }*/
    }
}
