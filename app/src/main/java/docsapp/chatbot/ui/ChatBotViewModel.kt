package docsapp.chatbot.ui

import androidx.lifecycle.*
import docsapp.chatbot.data.MessageRepository
import docsapp.chatbot.data.local.database.MessageEntity
import docsapp.core.Resource
import docsapp.core.State
import kotlinx.coroutines.launch

/**
 * Created by Vikas Gupta on 7/7/19.
 */
//To Handle UI Data

class ChatBotViewModel :ViewModel() {

    private var _messageList = MediatorLiveData<List<MessageEntity>>()

    val messageList:LiveData<List<MessageEntity>>
    get() = _messageList

    //for UI state
    private var _state = MediatorLiveData<State>()

    val state :LiveData<State>
    get() = _state

    fun sendMessage(msg:String,chatBotId: Long) {
        viewModelScope.launch {
            _state.addSource(MessageRepository.sendMessageAndGetReply(msg,false,chatBotId)){
                networkMsgResponse ->
                when(networkMsgResponse.status) {
                    Resource.Status.ERROR -> {
                        _state.postValue(State.ShowError(networkMsgResponse.error))
                    }
                }
            }
        }
    }

   fun subscribeMessageListStream(chatBotId:Long){
       viewModelScope.launch {
           _messageList.addSource(MessageRepository.getMessageListFromDb(chatBotId)){
               messageList ->
               _messageList.postValue(messageList)
           }
       }
   }
}