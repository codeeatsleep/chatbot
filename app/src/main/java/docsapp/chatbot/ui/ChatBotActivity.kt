package docsapp.chatbot.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import docsapp.chatbot.R

class ChatBotActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container,
                ChatBotListingFragment.newInstance()
            ).commit()
    }
}
