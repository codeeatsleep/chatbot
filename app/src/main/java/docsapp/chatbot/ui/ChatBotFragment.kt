package docsapp.chatbot.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import docsapp.chatbot.R
import docsapp.core.State
import kotlinx.android.synthetic.main.fragment_chat_bot.*
import kotlinx.android.synthetic.main.fragment_chat_bot.view.*

// chat window with bot

class ChatBotFragment : androidx.fragment.app.Fragment() {

    private lateinit var messageAdapter: MessageAdapter
    private lateinit var viewModel :ChatBotViewModel
    private  var chatBotId :Long = 0
    private lateinit var recyclerView:RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Chat Bot ID to whom you are communication
        chatBotId = arguments!!.getLong("id")

        val view = inflater.inflate(R.layout.fragment_chat_bot, container, false)
        recyclerView = view.message_list
        viewModel = ViewModelProviders.of(this).get(ChatBotViewModel::class.java)
        messageAdapter = MessageAdapter()
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = messageAdapter

        view.send_msg.setOnClickListener {
            viewModel.sendMessage(message.text.toString(),chatBotId)
            message.setText("")
        }

        //subscription to message stream
        viewModel.subscribeMessageListStream(chatBotId)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

/* Fragments can be detached and re-attached. When they are detached, their view hierarchy is destroyed and they become invisible and inactive, but their instance is not destroyed.
for more info :: https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
* */
        //get the latest message data as soon  as updated in db
        viewModel.messageList.observe(viewLifecycleOwner, Observer {
                messageList ->
            messageAdapter.messageList = messageList
            messageAdapter.notifyDataSetChanged()
            recyclerView.scrollToPosition(messageList.size-1)
        })

        // sometime i face error while processing request so i added //show error state on Ui
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when(it){
                is State.ShowError ->
                    Snackbar.make(message,it.errorMsg ?: "NA",Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(id: Long ) =
            ChatBotFragment().apply {
                arguments = Bundle().apply {
                    putLong("id", id)
                }
            }
    }


}
