package docsapp.chatbot.data.remote.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import docsapp.core.remote.BaseResponse

@JsonClass(generateAdapter = true)
data class MessageReplyResponse(

	@Json(name="data")
	val data: List<Any?>? = null,

	@Json(name="message")
	val message: Message

) : BaseResponse()