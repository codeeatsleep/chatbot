package docsapp.chatbot.data.remote.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Message(

	@Json(name="chatBotName")
	val chatBotName: String,

	@Json(name="emotion")
	val emotion: String? = null,

	@Json(name="chatBotID")
	val chatBotID: Int,

	@Json(name="message")
	val message: String
)