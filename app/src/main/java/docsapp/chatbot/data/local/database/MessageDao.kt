package docsapp.chatbot.data.local.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import docsapp.core.database.BaseDao

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@Dao
abstract class MessageDao : BaseDao<MessageEntity>() {

    @Query("SELECT * FROM Message WHERE chaBotId =:chatBotId ORDER BY createdOn")
    abstract fun getMessageEntitys(chatBotId:Long): LiveData<List<MessageEntity>>

    @Query("SELECT * FROM Message WHERE   NOT isSentFlag")
    abstract fun getNotSyncMessages(): List<MessageEntity>

    @Query("SELECT * FROM Message WHERE   id =:id")
    abstract fun getMessageEntity(id:Long): MessageEntity

}