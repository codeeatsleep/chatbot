package docsapp.chatbot.data.local.ui

/**
 * Created by Vikas Gupta on 7/7/19.
 */
data class Message(val message:String?,val time:Long,val isReceived:Boolean)