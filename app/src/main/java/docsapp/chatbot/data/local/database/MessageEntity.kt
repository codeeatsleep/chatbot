package docsapp.chatbot.data.local.database

import androidx.room.Entity
import docsapp.core.database.AbstractEntity

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@Entity(tableName = "Message")
class MessageEntity(
    var chaBotId:Long,
    var message:String,
    var isReceived:Boolean
) :AbstractEntity(isReceived)