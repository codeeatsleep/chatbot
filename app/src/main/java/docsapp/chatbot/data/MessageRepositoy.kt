package docsapp.chatbot.data

import androidx.lifecycle.LiveData
import docsapp.chatbot.data.local.database.MessageEntity
import docsapp.chatbot.data.remote.models.MessageReplyResponse
import docsapp.core.NetworkResource
import docsapp.core.Resource
import docsapp.core.database.ChatBotDataBase
import docsapp.core.remote.RestClient
import docsapp.core.util.Constants
import kotlinx.coroutines.*

/**
 * Created by Vikas Gupta on 8/7/19.
 */
// to provide data to view model either from local or remote source

object MessageRepository {

    private suspend fun getMessageResponse(message:String, id:String, chatBotId:Long, messageId:Long) : LiveData<Resource<MessageReplyResponse>> {

        return object : NetworkResource<MessageReplyResponse>(){
            override suspend fun saveReplyInDb(resultType: MessageReplyResponse) {
                    withContext(Dispatchers.Default){
                        sendMessage(resultType.message.message,true,chatBotId)
                        var messageEntity = ChatBotDataBase.getInstance().MessageDao().getMessageEntity(messageId)
                        messageEntity.isSentFlag = true
                        updateMessage(messageEntity)
                    }
                }
            //}
            override suspend fun createCallAsync(): Deferred<MessageReplyResponse> {
            return    RestClient.apiRestService.
                    getMessageReplyAsync(Constants.API_KEY,Constants.CHAT_BOT_ID,id,message)
            }

        }.build().asLiveData()
    }

   suspend fun sendMessage(message: String, isReceived:Boolean, chatBotId:Long) : Long = withContext(Dispatchers.Default){
       val messageEntity = MessageEntity(chatBotId, message, isReceived)
       ChatBotDataBase.getInstance().MessageDao().insert(messageEntity)

    }

    suspend fun sendMessageAndGetReply(message: String, isReceived:Boolean, chatBotId:Long):LiveData<Resource<MessageReplyResponse>> {
         val messageId = sendMessage(message,isReceived,chatBotId)
        return getMessageResponse(message,"1a",chatBotId,messageId)
    }

    suspend fun getMessageListFromDb(chatBotId:Long):LiveData<List<MessageEntity>> {
      return  withContext(Dispatchers.Default){
            ChatBotDataBase.getInstance().MessageDao().getMessageEntitys(chatBotId)
        }

    }

    suspend fun getNotSyncMessages():List<MessageEntity> {
       return withContext(Dispatchers.Default){
            ChatBotDataBase.getInstance().MessageDao().getNotSyncMessages()
        }
    }
    suspend fun updateMessage(messageEntity: MessageEntity){
        withContext(Dispatchers.Default){
            ChatBotDataBase.getInstance().MessageDao().update(messageEntity)
        }
    }
}