package docsapp.chatbot

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import docsapp.chatbot.data.MessageRepository
import docsapp.core.remote.RestClient
import docsapp.core.util.Constants

/**
 * Created by Vikas Gupta on 8/7/19.
 */
class OfflineMessageWorkManager(private val context: Context, params: WorkerParameters): CoroutineWorker(context,params) {

    override suspend fun doWork(): Result {
        val messageList = MessageRepository.getNotSyncMessages()
        messageList.forEach {
            messageEntity ->
            val result = RestClient.apiRestService.getMessageReplyAsync(Constants.API_KEY, Constants.CHAT_BOT_ID,"abc",messageEntity.message).await()
            if(result.success == 0) {
                return Result.failure()
            } else {
                messageEntity.isSentFlag = true
                MessageRepository.updateMessage(messageEntity)
                MessageRepository.sendMessage(result.message.message,true,messageEntity.chaBotId)
            }
    }
        return Result.success()
    }
}