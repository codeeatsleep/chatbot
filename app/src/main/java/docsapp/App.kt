package docsapp

import android.app.Application
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.facebook.stetho.Stetho
import docsapp.chatbot.BuildConfig
import docsapp.chatbot.OfflineMessageWorkManager
import java.util.concurrent.TimeUnit

/**
 * Created by Vikas Gupta on 8/7/19.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

    //for network call debuging
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }

        val work = PeriodicWorkRequestBuilder<OfflineMessageWorkManager>(
            50,
            TimeUnit.SECONDS
        )
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build())
            .addTag("OfflineMessageWorkManager")
            .build()
        WorkManager.getInstance().enqueue(work)
    }

    companion object {
        private  lateinit var instance: App
        fun get():App{
            return instance
        }
    }
}