package docsapp.core.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import docsapp.App
import docsapp.chatbot.data.local.database.MessageDao
import docsapp.chatbot.data.local.database.MessageEntity

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@Database(entities = arrayOf(MessageEntity::class), version = 1)
abstract class ChatBotDataBase :RoomDatabase() {

    abstract fun MessageDao(): MessageDao

    companion object {

        private var INSTANCE: ChatBotDataBase? = null

        private val lock = Any()

        fun getInstance(): ChatBotDataBase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        App.get(),
                        ChatBotDataBase::class.java, "chatbot.db"
                    )
                        .build()
                }
                return INSTANCE!!
            }
        }
    }
}