package docsapp.core.database

/**
 * Created by Vikas Gupta on 8/7/19.
 */

import androidx.room.*

@Dao
abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    abstract suspend fun insert(obj: T): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract  fun update(obj: T): Int

}