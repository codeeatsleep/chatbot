package docsapp.core.database

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import java.util.*

/**
 * Created by Vikas Gupta on 8/7/19.
 */
abstract class AbstractEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
    var createdOn:Long = Calendar.getInstance().timeInMillis
    var isSentFlag:Boolean = false

    constructor(isSentFlag:Boolean) {
        this.isSentFlag = isSentFlag
    }
}

