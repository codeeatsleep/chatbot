package docsapp.core

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import docsapp.core.remote.BaseResponse
import kotlinx.coroutines.*

/**
 * Created by Vikas Gupta on 8/7/19.
 */
abstract class NetworkResource<ResultType : BaseResponse> {

    private   var result = MutableLiveData<Resource<ResultType>>()

    suspend fun build():NetworkResource<ResultType> {
        CoroutineScope(Dispatchers.IO).launch {

            try {
                fetchFromNetwork()
            } catch (e: Exception) {
                Log.e("NetworkBoundResource", "An error happened: $e")
                result.postValue(Resource.error(e.message, null))
            }
        }
     return this
    }

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    // ---

    private suspend fun fetchFromNetwork() {
        Log.d(NetworkResource::class.java.name, "Fetch data from network")
        val apiResponse = createCallAsync().await()
        Log.e(NetworkResource::class.java.name, "Data fetched from network")
        val response = apiResponse as BaseResponse
        if(response.success == 1) {
            saveReplyInDb(apiResponse)
        } else {
            result.postValue(Resource.error(response.errorMessage, null))
        }
    }

    @WorkerThread
    protected abstract suspend fun createCallAsync(): Deferred<ResultType>

    @WorkerThread
    protected abstract suspend fun  saveReplyInDb(resultType: ResultType)
}