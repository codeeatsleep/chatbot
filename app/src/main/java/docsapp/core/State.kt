package docsapp.core

/**
 * Created by Vikas Gupta on 7/7/19.
 */
sealed class State {
    object Success : State()
    data class ShowError(val errorMsg: String?) : State()
}