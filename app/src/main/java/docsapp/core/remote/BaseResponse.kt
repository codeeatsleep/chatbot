package docsapp.core.remote

import com.squareup.moshi.Json

/**
 * Created by Vikas Gupta on 7/7/19.
 */
open class BaseResponse {

    @Json(name="success")
    var success: Int = 0

    @Json(name="errorMessage")
    var errorMessage: String? = null
}