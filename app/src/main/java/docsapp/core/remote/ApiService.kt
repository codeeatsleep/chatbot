package docsapp.core.remote

import docsapp.chatbot.data.remote.models.MessageReplyResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Vikas Gupta on 6/7/19.
 */
interface ApiService {

    @GET("chat/")
    fun getMessageReplyAsync(@Query("apiKey") apiKey: String, @Query("chatBotID") chatBotId: Int, @Query("externalID") externalId: String, @Query("message") message: String) : Deferred<MessageReplyResponse>

}