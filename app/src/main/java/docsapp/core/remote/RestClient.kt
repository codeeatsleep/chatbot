package docsapp.core.remote

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient

/**
 * Created by Vikas Gupta on 6/7/19.
 */
object RestClient {
    val API_URL = "https://www.personalityforge.com/api/"
    var apiRestService: ApiService
    init {
        val client = OkHttpClient.Builder().build()
        val moshiBuilder = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val retrofit2 = retrofit2.Retrofit.Builder()
            .baseUrl(API_URL)
            .client(client)
            .addCallAdapterFactory(com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory())
            .addConverterFactory(retrofit2.converter.moshi.MoshiConverterFactory.create(moshiBuilder))
            .build()
        apiRestService = retrofit2.create(ApiService::class.java)
    }

}