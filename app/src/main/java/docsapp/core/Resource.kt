package docsapp.core

/**
 * Created by Vikas Gupta on 8/7/19.
 */

data class Resource<out T>(val status: Status, val data: T?, val error: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(error: String?, data: T?): Resource<T> {
            return Resource(
                Status.ERROR,
                null,
                error
            )
        }
    }

    enum class Status {
        SUCCESS,
        ERROR
    }
}