package docsapp.chatbot.data.local.database;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.coroutines.Continuation;

@SuppressWarnings({"unchecked", "deprecation"})
public final class MessageDao_Impl extends MessageDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMessageEntity;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfMessageEntity;

  public MessageDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMessageEntity = new EntityInsertionAdapter<MessageEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `Message`(`chaBotId`,`message`,`isReceived`,`id`,`createdOn`,`isSentFlag`) VALUES (?,?,?,nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MessageEntity value) {
        stmt.bindLong(1, value.getChaBotId());
        if (value.getMessage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getMessage());
        }
        final int _tmp;
        _tmp = value.isReceived() ? 1 : 0;
        stmt.bindLong(3, _tmp);
        stmt.bindLong(4, value.getId());
        stmt.bindLong(5, value.getCreatedOn());
        final int _tmp_1;
        _tmp_1 = value.isSentFlag() ? 1 : 0;
        stmt.bindLong(6, _tmp_1);
      }
    };
    this.__updateAdapterOfMessageEntity = new EntityDeletionOrUpdateAdapter<MessageEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `Message` SET `chaBotId` = ?,`message` = ?,`isReceived` = ?,`id` = ?,`createdOn` = ?,`isSentFlag` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MessageEntity value) {
        stmt.bindLong(1, value.getChaBotId());
        if (value.getMessage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getMessage());
        }
        final int _tmp;
        _tmp = value.isReceived() ? 1 : 0;
        stmt.bindLong(3, _tmp);
        stmt.bindLong(4, value.getId());
        stmt.bindLong(5, value.getCreatedOn());
        final int _tmp_1;
        _tmp_1 = value.isSentFlag() ? 1 : 0;
        stmt.bindLong(6, _tmp_1);
        stmt.bindLong(7, value.getId());
      }
    };
  }

  @Override
  public Object insert(final MessageEntity obj, final Continuation<? super Long> p1) {
    return CoroutinesRoom.execute(__db, true, new Callable<Long>() {
      @Override
      public Long call() throws Exception {
        __db.beginTransaction();
        try {
          long _result = __insertionAdapterOfMessageEntity.insertAndReturnId(obj);
          __db.setTransactionSuccessful();
          return _result;
        } finally {
          __db.endTransaction();
        }
      }
    }, p1);
  }

  @Override
  public int update(final MessageEntity obj) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfMessageEntity.handle(obj);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<MessageEntity>> getMessageEntitys(final long chatBotId) {
    final String _sql = "SELECT * FROM Message WHERE chaBotId =? ORDER BY createdOn";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, chatBotId);
    return __db.getInvalidationTracker().createLiveData(new String[]{"Message"}, false, new Callable<List<MessageEntity>>() {
      @Override
      public List<MessageEntity> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfChaBotId = CursorUtil.getColumnIndexOrThrow(_cursor, "chaBotId");
          final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
          final int _cursorIndexOfIsReceived = CursorUtil.getColumnIndexOrThrow(_cursor, "isReceived");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfCreatedOn = CursorUtil.getColumnIndexOrThrow(_cursor, "createdOn");
          final int _cursorIndexOfIsSentFlag = CursorUtil.getColumnIndexOrThrow(_cursor, "isSentFlag");
          final List<MessageEntity> _result = new ArrayList<MessageEntity>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final MessageEntity _item;
            final long _tmpChaBotId;
            _tmpChaBotId = _cursor.getLong(_cursorIndexOfChaBotId);
            final String _tmpMessage;
            _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
            final boolean _tmpIsReceived;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsReceived);
            _tmpIsReceived = _tmp != 0;
            _item = new MessageEntity(_tmpChaBotId,_tmpMessage,_tmpIsReceived);
            final long _tmpId;
            _tmpId = _cursor.getLong(_cursorIndexOfId);
            _item.setId(_tmpId);
            final long _tmpCreatedOn;
            _tmpCreatedOn = _cursor.getLong(_cursorIndexOfCreatedOn);
            _item.setCreatedOn(_tmpCreatedOn);
            final boolean _tmpIsSentFlag;
            final int _tmp_1;
            _tmp_1 = _cursor.getInt(_cursorIndexOfIsSentFlag);
            _tmpIsSentFlag = _tmp_1 != 0;
            _item.setSentFlag(_tmpIsSentFlag);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public List<MessageEntity> getNotSyncMessages() {
    final String _sql = "SELECT * FROM Message WHERE   NOT isSentFlag";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfChaBotId = CursorUtil.getColumnIndexOrThrow(_cursor, "chaBotId");
      final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
      final int _cursorIndexOfIsReceived = CursorUtil.getColumnIndexOrThrow(_cursor, "isReceived");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCreatedOn = CursorUtil.getColumnIndexOrThrow(_cursor, "createdOn");
      final int _cursorIndexOfIsSentFlag = CursorUtil.getColumnIndexOrThrow(_cursor, "isSentFlag");
      final List<MessageEntity> _result = new ArrayList<MessageEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final MessageEntity _item;
        final long _tmpChaBotId;
        _tmpChaBotId = _cursor.getLong(_cursorIndexOfChaBotId);
        final String _tmpMessage;
        _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
        final boolean _tmpIsReceived;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsReceived);
        _tmpIsReceived = _tmp != 0;
        _item = new MessageEntity(_tmpChaBotId,_tmpMessage,_tmpIsReceived);
        final long _tmpId;
        _tmpId = _cursor.getLong(_cursorIndexOfId);
        _item.setId(_tmpId);
        final long _tmpCreatedOn;
        _tmpCreatedOn = _cursor.getLong(_cursorIndexOfCreatedOn);
        _item.setCreatedOn(_tmpCreatedOn);
        final boolean _tmpIsSentFlag;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfIsSentFlag);
        _tmpIsSentFlag = _tmp_1 != 0;
        _item.setSentFlag(_tmpIsSentFlag);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public MessageEntity getMessageEntity(final long id) {
    final String _sql = "SELECT * FROM Message WHERE   id =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfChaBotId = CursorUtil.getColumnIndexOrThrow(_cursor, "chaBotId");
      final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
      final int _cursorIndexOfIsReceived = CursorUtil.getColumnIndexOrThrow(_cursor, "isReceived");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCreatedOn = CursorUtil.getColumnIndexOrThrow(_cursor, "createdOn");
      final int _cursorIndexOfIsSentFlag = CursorUtil.getColumnIndexOrThrow(_cursor, "isSentFlag");
      final MessageEntity _result;
      if(_cursor.moveToFirst()) {
        final long _tmpChaBotId;
        _tmpChaBotId = _cursor.getLong(_cursorIndexOfChaBotId);
        final String _tmpMessage;
        _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
        final boolean _tmpIsReceived;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsReceived);
        _tmpIsReceived = _tmp != 0;
        _result = new MessageEntity(_tmpChaBotId,_tmpMessage,_tmpIsReceived);
        final long _tmpId;
        _tmpId = _cursor.getLong(_cursorIndexOfId);
        _result.setId(_tmpId);
        final long _tmpCreatedOn;
        _tmpCreatedOn = _cursor.getLong(_cursorIndexOfCreatedOn);
        _result.setCreatedOn(_tmpCreatedOn);
        final boolean _tmpIsSentFlag;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfIsSentFlag);
        _tmpIsSentFlag = _tmp_1 != 0;
        _result.setSentFlag(_tmpIsSentFlag);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
