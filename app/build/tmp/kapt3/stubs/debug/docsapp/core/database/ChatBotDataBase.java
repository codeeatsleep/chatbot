package docsapp.core.database;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@androidx.room.Database(entities = {docsapp.chatbot.data.local.database.MessageEntity.class}, version = 1)
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0006"}, d2 = {"Ldocsapp/core/database/ChatBotDataBase;", "Landroidx/room/RoomDatabase;", "()V", "MessageDao", "Ldocsapp/chatbot/data/local/database/MessageDao;", "Companion", "app_debug"})
public abstract class ChatBotDataBase extends androidx.room.RoomDatabase {
    private static docsapp.core.database.ChatBotDataBase INSTANCE;
    private static final java.lang.Object lock = null;
    public static final docsapp.core.database.ChatBotDataBase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract docsapp.chatbot.data.local.database.MessageDao MessageDao();
    
    public ChatBotDataBase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0006\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Ldocsapp/core/database/ChatBotDataBase$Companion;", "", "()V", "INSTANCE", "Ldocsapp/core/database/ChatBotDataBase;", "lock", "getInstance", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final docsapp.core.database.ChatBotDataBase getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}