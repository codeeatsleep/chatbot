package docsapp.core.util;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Ldocsapp/core/util/Constants;", "", "()V", "API_KEY", "", "CHAT_BOT_ID", "", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_KEY = "6nt5d1nJHkqbkphe";
    public static final int CHAT_BOT_ID = 63906;
    public static final docsapp.core.util.Constants INSTANCE = null;
    
    private Constants() {
        super();
    }
}