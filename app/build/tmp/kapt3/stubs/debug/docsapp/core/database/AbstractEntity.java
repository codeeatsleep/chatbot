package docsapp.core.database;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\n\b&\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0002\u0010\u000e\"\u0004\b\u000f\u0010\u0004\u00a8\u0006\u0010"}, d2 = {"Ldocsapp/core/database/AbstractEntity;", "", "isSentFlag", "", "(Z)V", "createdOn", "", "getCreatedOn", "()J", "setCreatedOn", "(J)V", "id", "getId", "setId", "()Z", "setSentFlag", "app_debug"})
public abstract class AbstractEntity {
    @androidx.room.ColumnInfo(name = "id")
    @androidx.room.PrimaryKey(autoGenerate = true)
    private long id;
    private long createdOn;
    private boolean isSentFlag;
    
    public final long getId() {
        return 0L;
    }
    
    public final void setId(long p0) {
    }
    
    public final long getCreatedOn() {
        return 0L;
    }
    
    public final void setCreatedOn(long p0) {
    }
    
    public final boolean isSentFlag() {
        return false;
    }
    
    public final void setSentFlag(boolean p0) {
    }
    
    public AbstractEntity(boolean isSentFlag) {
        super();
    }
}