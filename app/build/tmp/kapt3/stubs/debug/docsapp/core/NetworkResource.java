package docsapp.core;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00070\tJ\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\rH%J\u0011\u0010\u000e\u001a\u00020\u000fH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00028\u0000H%\u00a2\u0006\u0002\u0010\u0012R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0013"}, d2 = {"Ldocsapp/core/NetworkResource;", "ResultType", "Ldocsapp/core/remote/BaseResponse;", "", "()V", "result", "Landroidx/lifecycle/MutableLiveData;", "Ldocsapp/core/Resource;", "asLiveData", "Landroidx/lifecycle/LiveData;", "build", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "createCallAsync", "Lkotlinx/coroutines/Deferred;", "fetchFromNetwork", "", "saveReplyInDb", "resultType", "(Ldocsapp/core/remote/BaseResponse;)V", "app_debug"})
public abstract class NetworkResource<ResultType extends docsapp.core.remote.BaseResponse> {
    private androidx.lifecycle.MutableLiveData<docsapp.core.Resource<ResultType>> result;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object build(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super docsapp.core.NetworkResource<ResultType>> p0) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<docsapp.core.Resource<ResultType>> asLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.WorkerThread()
    protected abstract kotlinx.coroutines.Deferred<ResultType> createCallAsync();
    
    @androidx.annotation.WorkerThread()
    protected abstract void saveReplyInDb(@org.jetbrains.annotations.NotNull()
    ResultType resultType);
    
    public NetworkResource() {
        super();
    }
}