package docsapp.core.remote;

import java.lang.System;

/**
 * Created by Vikas Gupta on 7/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R&\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR$\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\f\u0010\u0002\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Ldocsapp/core/remote/BaseResponse;", "", "()V", "errorMessage", "", "errorMessage$annotations", "getErrorMessage", "()Ljava/lang/String;", "setErrorMessage", "(Ljava/lang/String;)V", "success", "", "success$annotations", "getSuccess", "()I", "setSuccess", "(I)V", "app_debug"})
public class BaseResponse {
    private int success;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String errorMessage;
    
    @com.squareup.moshi.Json(name = "success")
    public static void success$annotations() {
    }
    
    public final int getSuccess() {
        return 0;
    }
    
    public final void setSuccess(int p0) {
    }
    
    @com.squareup.moshi.Json(name = "errorMessage")
    public static void errorMessage$annotations() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getErrorMessage() {
        return null;
    }
    
    public final void setErrorMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public BaseResponse() {
        super();
    }
}