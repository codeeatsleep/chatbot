package docsapp.core.remote;

import java.lang.System;

/**
 * Created by Vikas Gupta on 6/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f\u00a8\u0006\r"}, d2 = {"Ldocsapp/core/remote/RestClient;", "", "()V", "API_URL", "", "getAPI_URL", "()Ljava/lang/String;", "apiRestService", "Ldocsapp/core/remote/ApiService;", "getApiRestService", "()Ldocsapp/core/remote/ApiService;", "setApiRestService", "(Ldocsapp/core/remote/ApiService;)V", "app_debug"})
public final class RestClient {
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String API_URL = "https://www.personalityforge.com/api/";
    @org.jetbrains.annotations.NotNull()
    private static docsapp.core.remote.ApiService apiRestService;
    public static final docsapp.core.remote.RestClient INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAPI_URL() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final docsapp.core.remote.ApiService getApiRestService() {
        return null;
    }
    
    public final void setApiRestService(@org.jetbrains.annotations.NotNull()
    docsapp.core.remote.ApiService p0) {
    }
    
    private RestClient() {
        super();
    }
}