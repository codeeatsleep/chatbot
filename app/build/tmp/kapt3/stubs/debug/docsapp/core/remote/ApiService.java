package docsapp.core.remote;

import java.lang.System;

/**
 * Created by Vikas Gupta on 6/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J6\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u0006H\'\u00a8\u0006\u000b"}, d2 = {"Ldocsapp/core/remote/ApiService;", "", "getMessageReplyAsync", "Lkotlinx/coroutines/Deferred;", "Ldocsapp/chatbot/data/remote/models/MessageReplyResponse;", "apiKey", "", "chatBotId", "", "externalId", "message", "app_debug"})
public abstract interface ApiService {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "chat/")
    public abstract kotlinx.coroutines.Deferred<docsapp.chatbot.data.remote.models.MessageReplyResponse> getMessageReplyAsync(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "apiKey")
    java.lang.String apiKey, @retrofit2.http.Query(value = "chatBotID")
    int chatBotId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "externalID")
    java.lang.String externalId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "message")
    java.lang.String message);
}