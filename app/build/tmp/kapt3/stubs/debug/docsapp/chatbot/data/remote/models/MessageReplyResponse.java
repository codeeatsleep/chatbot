package docsapp.chatbot.data.remote.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B#\u0012\u0012\b\u0003\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0013\u0010\f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0012\b\u0003\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u001b\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Ldocsapp/chatbot/data/remote/models/MessageReplyResponse;", "Ldocsapp/core/remote/BaseResponse;", "data", "", "", "message", "Ldocsapp/chatbot/data/remote/models/Message;", "(Ljava/util/List;Ldocsapp/chatbot/data/remote/models/Message;)V", "getData", "()Ljava/util/List;", "getMessage", "()Ldocsapp/chatbot/data/remote/models/Message;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class MessageReplyResponse extends docsapp.core.remote.BaseResponse {
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.Object> data = null;
    @org.jetbrains.annotations.NotNull()
    private final docsapp.chatbot.data.remote.models.Message message = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.Object> getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final docsapp.chatbot.data.remote.models.Message getMessage() {
        return null;
    }
    
    public MessageReplyResponse(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "data")
    java.util.List<? extends java.lang.Object> data, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "message")
    docsapp.chatbot.data.remote.models.Message message) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.Object> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final docsapp.chatbot.data.remote.models.Message component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final docsapp.chatbot.data.remote.models.MessageReplyResponse copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "data")
    java.util.List<? extends java.lang.Object> data, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "message")
    docsapp.chatbot.data.remote.models.Message message) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}