package docsapp.chatbot.ui;

import java.lang.System;

/**
 * Created by Vikas Gupta on 7/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\f\u00a8\u0006\u0016"}, d2 = {"Ldocsapp/chatbot/ui/ChatBotViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_messageList", "Landroidx/lifecycle/MediatorLiveData;", "", "Ldocsapp/chatbot/data/local/database/MessageEntity;", "_state", "Ldocsapp/core/State;", "messageList", "Landroidx/lifecycle/LiveData;", "getMessageList", "()Landroidx/lifecycle/LiveData;", "state", "getState", "sendMessage", "", "msg", "", "chatBotId", "", "subscribeMessageListStream", "app_debug"})
public final class ChatBotViewModel extends androidx.lifecycle.ViewModel {
    private androidx.lifecycle.MediatorLiveData<java.util.List<docsapp.chatbot.data.local.database.MessageEntity>> _messageList;
    private androidx.lifecycle.MediatorLiveData<docsapp.core.State> _state;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<docsapp.chatbot.data.local.database.MessageEntity>> getMessageList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<docsapp.core.State> getState() {
        return null;
    }
    
    public final void sendMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String msg, long chatBotId) {
    }
    
    public final void subscribeMessageListStream(long chatBotId) {
    }
    
    public ChatBotViewModel() {
        super();
    }
}