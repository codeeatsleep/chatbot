package docsapp.chatbot.data.local.database;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\b\'\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u001c\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u0006H\'J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\tH\'\u00a8\u0006\f"}, d2 = {"Ldocsapp/chatbot/data/local/database/MessageDao;", "Ldocsapp/core/database/BaseDao;", "Ldocsapp/chatbot/data/local/database/MessageEntity;", "()V", "getMessageEntity", "id", "", "getMessageEntitys", "Landroidx/lifecycle/LiveData;", "", "chatBotId", "getNotSyncMessages", "app_debug"})
public abstract class MessageDao extends docsapp.core.database.BaseDao<docsapp.chatbot.data.local.database.MessageEntity> {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM Message WHERE chaBotId =:chatBotId ORDER BY createdOn")
    public abstract androidx.lifecycle.LiveData<java.util.List<docsapp.chatbot.data.local.database.MessageEntity>> getMessageEntitys(long chatBotId);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM Message WHERE   NOT isSentFlag")
    public abstract java.util.List<docsapp.chatbot.data.local.database.MessageEntity> getNotSyncMessages();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM Message WHERE   id =:id")
    public abstract docsapp.chatbot.data.local.database.MessageEntity getMessageEntity(long id);
    
    public MessageDao() {
        super();
    }
}