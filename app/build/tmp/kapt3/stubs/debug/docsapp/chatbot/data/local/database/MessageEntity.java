package docsapp.chatbot.data.local.database;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@androidx.room.Entity(tableName = "Message")
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0007\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u00a8\u0006\u0014"}, d2 = {"Ldocsapp/chatbot/data/local/database/MessageEntity;", "Ldocsapp/core/database/AbstractEntity;", "chaBotId", "", "message", "", "isReceived", "", "(JLjava/lang/String;Z)V", "getChaBotId", "()J", "setChaBotId", "(J)V", "()Z", "setReceived", "(Z)V", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "app_debug"})
public final class MessageEntity extends docsapp.core.database.AbstractEntity {
    private long chaBotId;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String message;
    private boolean isReceived;
    
    public final long getChaBotId() {
        return 0L;
    }
    
    public final void setChaBotId(long p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final boolean isReceived() {
        return false;
    }
    
    public final void setReceived(boolean p0) {
    }
    
    public MessageEntity(long chaBotId, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean isReceived) {
        super(false);
    }
}