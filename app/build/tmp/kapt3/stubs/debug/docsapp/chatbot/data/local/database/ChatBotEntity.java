package docsapp.chatbot.data.local.database;

import java.lang.System;

/**
 * Created by Vikas Gupta on 8/7/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Ldocsapp/chatbot/data/local/database/ChatBotEntity;", "", "()V", "app_debug"})
public final class ChatBotEntity {
    
    public ChatBotEntity() {
        super();
    }
}